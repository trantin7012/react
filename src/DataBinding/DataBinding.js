import React, { Component } from 'react';

class DataBinding extends Component {
    userName = 'Tin fe38'

    hocVien = {
        maHv:1,
        tenHV:'Nguyễn văn a',
        tuoi:19
    }

    renderImg= () =>{
        // Khi binding data 
        return <img src="https://image.tienphong.vn/w1000/Uploaded/2020/ttf_ztmfxuzt/2020_04_25/facebook_thuong_thuong_gpnf.jpg"></img>
    }
    render() {
        let {maHv,tenHV,tuoi} = this.hocVien;
        const age =20;
        return (
            <div>
                {this.renderImg()}
                <p id= "text">{this.userName}</p>
        <p id="text">{age}</p>
            
            <ul>
                <li>mã: {maHv}</li>
                <li>tên: {tenHV}</li>
              <li>tuoi: {tuoi}</li>
            </ul>
            </div>
        );
    }
}

export default DataBinding;