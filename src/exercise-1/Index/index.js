import React, {Component} from 'react'
import Header from '../Header/header'
import Contents from'../Contents/contents'
import Slidebar from '../SlideBar/slidebar'



class Ex1 extends Component{
    render(){
        return(
            <div className="Head">
                <Header/>
                <div className="Body">
                  <Contents/>
                    <Slidebar/>
                 </div>
            </div>

        )
    }
}

export default Ex1