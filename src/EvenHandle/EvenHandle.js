import React, { Component } from 'react';

class EvenHandle extends Component {
    showMessage =()=>{
        alert("Hello 38")
    }
    showProfile = (profile) =>{
        console.log(profile);
    }

    showProfileCallback =(hocVien , _this) =>{
        console.log(hocVien)
        console.log(_this)
    }
     render() {
        let hocVien ={
            ma : 1,
            tên: 'Lê Văn Tèo',
            lop: 'fe 38'
        }
        return (
            <div>
                {/** Cách 1: xử lý sự kiện dùng funtion 1 lần  */}
                <button onClick={()=>{
                    alert("hahaha")
                }}>click me!!!</button>
                {/** Cách 2 xử lý sự kiện dùng funtion nhiều lần */}
                <button onClick={this.showMessage}> Click me 2 !!!</button>
                {/** Xử lý sự kiện với tham số  */}
                <button onClick={() =>{this.showProfile(hocVien)}}>show profile</button>

                <button onClick={this.showProfileCallback.bind(this,hocVien)}>click bind</button>
            </div>
        );
    }
}

export default EvenHandle;